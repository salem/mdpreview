#!/usr/bin/env python

# mdPreview
# Copyright 2013 Salem
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys
import markdown
from pyinotify import *
from datetime import datetime
import signal, logging, os

import util
from util import APPNAME, APPAUTHOR, APP_ROOT
import template as tmpl


class AppMainWindow(QMainWindow):

	filename = None
	last_update = datetime.now()
	trigger_update = pyqtSignal()
	trigger_drag_file = pyqtSignal([str])

	theme = {"code": "default", "main": "default"}

	def __init__(self, parent=None, filename=""):
		self.filename = filename
		logging.basicConfig(format="'%(asctime)-15s: %(message)s'")
		self.logger = logging.getLogger(APPNAME)
		self.logger.setLevel(logging.INFO)

		# Setup UI.
		QMainWindow.__init__(self, parent)
		self.setMinimumSize(350, 300)
		self.__create_menus()
		self.__load_geometry()

		webview = util.getPreview()
		webview.shareTrigger(self.trigger_drag_file)
		#webview.setStyleSheet("padding: 50px;")
		self.webview = webview
		self.setCentralWidget(webview)
		self.updateTitle()
		# Setup triggers.
		self.trigger_update.connect(self.updatePreview)
		self.trigger_drag_file.connect(self.loadFile)

		# Set up inotify
		self.wm = None
		if self.file_is_readable():
			self.setupInotify()

		# Load .md file and render it!
		self.updatePreview()

	def set_code_theme(self, theme):
		self.theme["code"] = str(theme).lower()
		self.updatePreview()

	def set_main_theme(self, theme):
		self.theme["main"] = str(theme).lower()
		self.updatePreview()

	def __load_geometry(self):
		settings = QSettings(APPAUTHOR, APPNAME)
		self.restoreGeometry(settings.value("geometry").toByteArray())
		self.restoreState(settings.value("windowState").toByteArray())

	def __create_menus(self):
		mb = self.menuBar()
		fileMenu = mb.addMenu("File")
		fileMenu.addAction(QAction("Open", fileMenu))
		# edit menu
		editMenu = mb.addMenu("Edit")
		# styles menu
		styleMenu = mb.addMenu("Style")
		main = styleMenu.addMenu("Main")

		code = styleMenu.addMenu("Code")

		# Main style
		sm_main = QSignalMapper(self)
		styles = util.get_main_styles()
		styles.sort()
		main_group = QActionGroup(main)
		for st in styles:
			a = QAction(st, main)
			a.setCheckable(True)
			sm_main.setMapping(a, st)
			if st == "Default":
				a.setChecked(True)
			a.triggered.connect(sm_main.map)
			main_group.addAction(a)
			main.addAction(a)
		sm_main.mapped[str].connect(self.set_main_theme)

		# Code style
		sm = QSignalMapper(self)
		styles = util.get_code_styles()
		styles.sort()
		code_group = QActionGroup(code)
		for st in styles:
			a = QAction(st, code)
			a.setCheckable(True)
			sm.setMapping(a, st)
			if st == "Default":
				a.setChecked(True)
			a.triggered.connect(sm.map)
			code_group.addAction(a)
			code.addAction(a)
		sm.mapped[str].connect(self.set_code_theme)

	def file_is_readable(self):
		return os.access(self.filename, os.O_RDONLY)

	def loadFile(self, filename):
		""" Method used to load a new file and update the UI. """
		oldfile = self.filename
		self.filename = str(filename)
		self.updateTitle()
		self.setupInotify(oldfile)
		self.updatePreview()

	def updateTitle(self):
		filename = self.filename
		if filename:
			title = "{0} - {1}".format(APPNAME, os.path.abspath(filename))
		else:
			title = APPNAME
		self.setWindowTitle(title)

	def setupInotify(self, oldfile=""):
		if not self.wm:
			self.logger.info("Creating WatchManager")
			self.wm = WatchManager()
			self.notifier = ThreadedNotifier(self.wm, self.process_IN_MODIFY)
			self.notifier.start()
		if oldfile:
			self.wm.rm_watch(self.wm.get_wd(oldfile))
			self.logger.info("Stoppped watching for changes: {0}".format(oldfile))
		self.wm.add_watch(self.filename, EventsCodes.OP_FLAGS["IN_MODIFY"])
		self.logger.info("Watching file for changes: {0}".format(self.filename))

	def updatePreview(self):
		self.logger.info("Updating HTML preview")
		now = datetime.now()
		#diff = now - self.last_update
		self.last_update = now
		self.html = self.renderHTML(self.filename)
		self.webview.setHtml(self.html)
		#self.last_update = datetime.now()

	def renderHTML(self, filename):
		if filename:
			try:
				md_data = open(filename, "r").read()
				inner_html = markdown.markdown(md_data,
					extensions=["toc", "fenced_code", "codehilite"],
					output_format="html5"
				)
			except IOError:
				inner_html = tmpl.HTML_ERROR.format(filename=self.filename)
		else:
			if self.webview.hasWebkit:
				inner_html = tmpl.HTML_NO_FILE.format(ROOT=APP_ROOT)
			else:
				inner_html = ""
		final_html = tmpl.HTML.format(html=inner_html, ROOT=APP_ROOT, STYLE=self.theme)
		return final_html

	def process_IN_MODIFY(self, event):
		""" Method used to process the modification events sent by inotify.
		This method is used by pyinotify, so it may be running in a different
		thread that from the main program. """
		# Emit signal to update QWebView ()
		self.trigger_update.emit()

	def closeEvent(self, event):
		""" Method called when the application is quitting. """
		# Just stop inotify thread to prevent hangup.
		try:
			if self.wm:
				self.notifier.stop()
				#self.wm.close()
			# Saving geometry and window position
			settings = QSettings(APPAUTHOR, APPNAME)
			settings.setValue("geometry", self.saveGeometry())
			settings.setValue("windowState", self.saveState())
		except Exception as e:
			self.logger.warn(e)
		self.logger.info("Shutting down")


def main(_filename):
	app = QApplication(sys.argv)
	app.setApplicationName(APPNAME)
	app.setWindowIcon(QIcon("media/icon.png"))
	window = AppMainWindow(filename=_filename)
	window.show()
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	sys.exit(app.exec_())


if __name__ == "__main__":
	try:
		filepath = sys.argv[1]
		main(filepath)
	except IndexError:
		main("")
