#!/usr/bin/env python

# mdPreview
# Copyright 2013 Salem
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt4.QtGui import QTextEdit
import logging
import os


APPAUTHOR = "salem"
APPNAME = "mdPreview"
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

try:
	from PyQt4.QtWebKit import QWebView

	class MyQWebView(QWebView):
		""" Subclass of QWebView that overrides the default drop method. """

		hasWebkit = True

		def shareTrigger(self, t):
			self.trigger = t

		def dropEvent(self, event):
			mime = event.mimeData()
			if mime.hasUrls():
				urls = mime.urls()
				url = urls[0].toString()
				url_final = url[7:]
				print "Got url: {0}".format(url_final)
				self.trigger.emit(url_final)
			else:
				event.ignore()
except:
	pass


class MyQTextEdit(QTextEdit):
	""" Subclass of QTextEdit that overrides the default drop method. """

	hasWebkit = False

	def shareTrigger(self, t):
		self.trigger = t

	def dropEvent(self, event):
		mime = event.mimeData()
		if mime.hasUrls():
			urls = mime.urls()
			url = urls[0].toString()
			url_final = url[7:]
			print "Got url: {0}".format(url_final)
			self.trigger.emit(url_final)
		else:
			event.ignore()


def getPreview():
	logger = logging.getLogger("mdPreview")
	try:
		preview = MyQWebView()
		logger.info("Using WebKit as renderer")
	except:
		preview = MyQTextEdit()
		preview.setAcceptRichText(True)
		preview.setReadOnly(True)
		logger.info("Using Qt as renderer")
	return preview


def get_code_styles():
	try:
		from pygments import styles
		return map(lambda x: x.capitalize(), styles.get_all_styles())
	except ImportError:
		return []


def get_main_styles():
	return ["Default", "Bootstrap"]
