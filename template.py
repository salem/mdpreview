
HTML = """<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<style>
	body {{
		margin: 10px 10px;
	}}
	h1 {{ }}
	.no_file {{
		background-color: rgba(125, 125, 125, 0.7);
		margin: auto auto;
		width: 300px;
		height: 300px;
		border: 5px dashed black;
		display: inline-block;
		font-size: 3em;
		border-radius: 30px;
		line-height: 100px;
		text-align: center;
		position: absolute;
		margin: 150px 0 0 -170px;
		padding: 10px;
		left: 50%;
		right: 50%;
	}}
	.error {{
		background-color: rgba(125, 0, 0, 0.7);
		margin: auto auto;
		width: 300px;
		height: 300px;
		border: 5px dashed black;
		display: inline-block;
		font-size: 3em;
		border-radius: 30px;
		line-height: 150px;
		text-align: center;
		position: absolute;
		margin: 150px 0 0 -150px;
		padding: 15px;
		left: 50%;
		right: 50%;
	}}
	</style>
	<link rel="stylesheet" href="file://{ROOT}/styles/main/{STYLE[main]}.css">
	<link rel="stylesheet" href="file://{ROOT}/styles/code/{STYLE[code]}.css">
	<title>gg</title>
</head>
<body>
{html}
</body>
</html>
"""

HTML_NO_FILE = '''
<div class="no_file" onmousedown="return false">
<img style="-webkit-user-select: none; width: 100%; height: 100%;" src="file://{ROOT}/media/drop.svg"></img>
</div>
'''


"""
<div class="no_file">
Drag your markdown file here
</div>
"""

HTML_ERROR = """
<div class="error">
Could not load file: {filename}
</div>
"""

ABOUT_DIALOG = """
<h3>mdPreview</h3>
<p>
A simple markdown previewer, with auto-reload capabilities.
</p>
"""
